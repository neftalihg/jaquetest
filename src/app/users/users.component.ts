import { Component, OnInit } from '@angular/core';
import { UsersService} from '../../services/users.service';
import { environment } from '../../environments/environment.prod';
import { User } from 'src/models/user';
import { Rol } from 'src/models/rol';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users : User[];
  roles : Rol[];
  assets =  environment.endPoint;
  showNewUserModal: boolean = false;
  showUpdateUserModal: boolean = false;
  constructor(private usersService: UsersService) { 
    
  }

  ngOnInit() {
    this.usersService.getUsers().subscribe(
      (data: any) => {
        this.users = data.users;
      },
      (error) => {
        console.log(error);
      }
    );
    this.usersService.getRoles().subscribe(
      (data: any) => {
        this.roles = data.roles;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  openNewUserModal(){
    this.showNewUserModal =true;
  }
  closeNewUserModal(){
    this.showNewUserModal =false;
  }
  openUpdateUserModal(){
    this.showUpdateUserModal =true;
  }
  closeUpdateUserModal(){
    this.showUpdateUserModal =false;
  }
}
