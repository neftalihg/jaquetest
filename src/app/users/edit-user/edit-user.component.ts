import { Component, OnInit, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { User } from 'src/models/user';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  @Input() showModal: boolean;
  @Input() currentUser: User;
  @Output() userUpdated:  EventEmitter<User>;
  @Output() closeModal:  EventEmitter<boolean>;
  

  user: User;
  constructor() { 
    this.closeModal = new EventEmitter();
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.user = this.currentUser;
    
  }
  closeModalUpdateUSer(){
    this.closeModal.emit(true);
  }

}
