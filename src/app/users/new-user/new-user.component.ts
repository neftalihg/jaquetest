import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../../../models/user';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  @Input() showModal: boolean;
  @Output() newUserCreated:  EventEmitter<User>;
  @Output() closeModal:  EventEmitter<boolean>;

  assets =  environment.endPoint;
  
  constructor() { 
    this.closeModal = new EventEmitter();
  }

  ngOnInit() {
  }

  closeModalNewUser(){
    this.closeModal.emit(true);
  }
}
