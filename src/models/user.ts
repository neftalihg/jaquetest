export class User {
    picture: String;
    name: String;
    fathersLastName: String;
    mothersLastName: String;
    email: String;
    roleId: number;
    active: Boolean;
}
