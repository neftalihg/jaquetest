import { Pipe, PipeTransform } from '@angular/core';
import { Rol } from 'src/models/rol';

@Pipe({
  name: 'roleName'
})
export class RoleNamePipe implements PipeTransform {

  transform(value: number, roles: Rol[]): any {
    if(roles){
      return roles[roles.findIndex(rol => rol.id == value)].position;
    }
    else{
      return "";
    }
  }

}
