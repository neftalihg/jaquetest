import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  endPoint = environment.endPoint;
  constructor(private http: HttpClient) { }

  getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Access-Control-Allow-Origin' : '*'
    });
    return headers;
  }

  getUsers() : Observable<any>{
    var headers = this.getHeaders();
    return this.http.get(`${this.endPoint}users.json`, {headers})
  }

  getRoles() : Observable<any>{
    var headers = this.getHeaders();
    return this.http.get(`${this.endPoint}roles.json`, {headers})
  }

}
